(ns clj.clj.direction
  (:require [clojure.tools.logging :as log]))



;https://maps.googleapis.com/maps/api/distancematrix/json?origins=75+9th+Ave+New+York,+NY&destinations=Bridgewater+Commons,+Commons+Way,+Bridgewater,+NJ|The+Mall+At+Short+Hills,+Morris+Turnpike,+Short+Hills,+NJ|Monmouth+Mall,+Eatontown,+NJ|Westfield+Garden+State+Plaza,+Garden+State+Plaza+Boulevard,+Paramus,+NJ|Newport+Centre+Mall,+Jersey+City,+NJ&departure_time=1541202457&traffic_model=best_guess&key=YOUR_API_KEY
(defn call-google [origin destination mode & via]
  (try
    (let [url "https://maps.googleapis.com/maps/api/distancematrix/json"
          key "AIzaSyAzC0U8qizejJcJAzq4xEmO723ImrO5gXo"
          others (apply str "|" via)
          waypoints (apply str "optimize:true"others)
          options  {:query-params {:mode mode
                                   :origins (encode-str origin)
                                   :destinations (encode-str destination)
                                   ; :waypoints waypoints
                                   :traffic_model "best_guess"
                                   :key key}
                    :headers {"Content-Type" "application/text"}}
          {:keys [status body error] :as trace}  @(http/get url options)]
      (log/infof "Google map request: %s" (apply str url options))
      (if error
        (let []
          (log/infof "Connection exception %s|%s" error trace))

        (let [body (. URLDecoder decode body)
              resp (json/read-str body :key-fn keyword)
              status (:status resp)
              start_address (:origin_addresses resp)
              end_address (:destination_addresses resp)
              rows (get (:rows resp) 0)
              elements (get (:elements rows) 0)
              distance (:distance elements)
              duration (:duration elements)]
          (log/info body)
          (log/infof "%s|Start address: %s|End address: %s" status start_address end_address)
          (log/infof "Distance: %s|Duration: %s" distance duration)

          ;(log/infof "Distance:%s | Duration:%s"distance duration)
          ;(log/infof "Parsed message %s" routes)
          )))
    (catch Exception e
      (log/errorf "!Google location map service:%s" e))))

