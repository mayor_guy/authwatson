(ns clj.core
  (:require [clojure.walk :as walk]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [org.httpkit.client :as http]
            [clj.service :as serv]
            [clj.autocomplete :as autocom]
            [clojure.set :as set])
  (:use [org.httpkit.server]
        [clojure.tools.logging :as log]
        [clj.rpc :as rpc])
  (:import
    (com.ibm.watson.developer_cloud.conversation.v1 ConversationService)
    (com.ibm.watson.developer_cloud.conversation.v1.model MessageRequest)
    (com.ibm.watson.developer_cloud.conversation.v1.model MessageResponse MessageRequest$Builder)
    (java.net URLEncoder URLDecoder)))

(declare encode-str)

(defn call-google-direction [origin destination mode & via]
    (let [url "https://maps.googleapis.com/maps/api/directions/json"
          key "AIzaSyAzC0U8qizejJcJAzq4xEmO723ImrO5gXo"
          others (apply str "|" via)
          waypoints (apply str "optimize:true"others)
          options  {:query-params {:mode mode :origin (encode-str origin)  :destination (encode-str destination)
                                   :waypoints waypoints
                                   :key key}
                    :headers {"Content-Type" "application/text"}}
          {:keys [status body error] :as trace}  @(http/get url options)]
      (log/infof "Google map request: %s" (apply str url options))
      (if error
        (let []
          (log/infof "Connection exception %s|%s" error trace))

        (let [bo (. URLDecoder decode body)
              resp (json/read-str bo :key-fn keyword)
              status (:status resp)
              routes (get (:routes resp) 0)
              legs (:legs routes)
              legs (get (:legs routes) 0)
              distance (:distance legs)

              duration (:duration legs)
              start_address (:start_address legs)
              start_location (:start_location legs)

              steps (:steps legs)
              end_address (:end_address legs)
              end_location (:end_location legs)]
          (log/info body)
          (log/infof "Start address: %s | End address: %s"start_address end_address)
          (log/infof "Start location: %s| End location: %s"start_location end_location)
          (log/infof "legs:%s" legs)
          (log/infof "Distance:%s | Duration:%s"distance duration)
          (log/infof "Steps:%s |%s " (count steps) steps)
          (log/infof "Timetaken: %s" (rpc/nestedloop steps :duration :text 0))
          (log/infof "Parsed message %s" routes)))))



(defn connect [input context & args]
  (try
    (let [username "4cc48715-694c-4b65-b673-fd163508c92e"
          password "ylbbhHHvA6lc"
          input input
          context context
          workspaceId
          "cc8b80b0-478c-4a1f-8eb3-7dabb673cfc6"
          ;"12f6ab47-23da-4be6-8b37-0b7896fa4070"
          service (ConversationService. "2017-08-26")
          userPass (.setUsernameAndPassword service username password)
          newMessage (-> (MessageRequest$Builder.) (.inputText input) (.context context) (.build))

          ;MessageRequest    newMessage = new MessageRequest.Builder().inputText(input).context(context).build();
          ;ussd_service_opt.getData().getHexDump()
          ;(.getHexDump (.getData ussd_service_tag)

          ;; msgReq (MessageRequest$Builder.)
          ;;inputText (.inputText msgReq input)
          ;;content (.context inputText @context)
          ;;newMessage (.build content)

          ;service.message(workspaceId, newMessage).execute();\
          body (.execute (.message service workspaceId newMessage))
          response (.toString body)
          ; input-vec (apply hash-map (str/split @context #"="))
          ;new_content(walk/keywordize-keys input-vec)
          resp (json/read-str response :key-fn keyword)
          cont(:context resp)]
      ;(reset! context cont)

      (log/infof  "Watson Response: %s" response)
      (log/infof "Watson parsed response %s"resp)
      (log/infof "Watson request: %s|%s" (:url (:request cont)) (:request cont))
      (log/infof "Watson entities: %s" (:entities cont))
      (log/infof "Watson intents: %s" (:intents cont))
      (log/infof  "Watson Context: %s"cont )
      (log/infof "Response: %s" (.getText body))
      (.getText body))

    (catch Exception e
      (log/errorf "!Conversation:%s"e))))



(defn encode-str [str]
  ;String encodedUrl = URLEncoder.encode(url, "UTF-8");
  (. URLEncoder encode str))



;https://maps.googleapis.com/maps/api/directions/json?origin=Palmgrove,Lagos&destination=1004+housing+estate&key=AIzaSyAzC0U8qizejJcJAzq4xEmO723ImrO5gXo
;https://maps.googleapis.com/maps/api/directions/json?origin=Boston,MA&destination=Concord,MA&waypoints=Charlestown,MA|via:Lexington,MA&key=YOUR_API_KEY
(defn call-google [origin destination mode & via]
  (try
    (let [url "https://maps.googleapis.com/maps/api/distancematrix/json"
          key "AIzaSyAzC0U8qizejJcJAzq4xEmO723ImrO5gXo"
          others (apply str "|" via)
          waypoints (apply str "optimize:true"others)
          options  {:query-params {:mode mode
                                   :departure_time "now"
                                   :origins (encode-str origin)
                                   :destinations (encode-str destination)
                                   :waypoints waypoints
                                   :key key}
                    :headers {"Content-Type" "application/text"}}
          {:keys [status body error] :as trace}  @(http/get url options)]
      (log/infof "Google map request: %s" (apply str url options))
      (if error
        (let []
          (log/infof "Connection exception %s|%s" error trace))

        (let [body (. URLDecoder decode body)
              resp (json/read-str body :key-fn keyword)
              status (:status resp)
              start_address (:origin_addresses resp)
              end_address (:destination_addresses resp)
              rows (get (:rows resp) 0)
              elements (get (:elements rows) 0)
              distance (:distance elements)
              duration (:duration elements)
              duration_in_traffic (:duration_in_traffic elements)]
          (log/info body)
          (log/infof "%s|Start address: %s|End address: %s" status start_address end_address)
          (log/infof "Distance: %s|Duration: %s|Duration in Traffic:%s" distance duration duration_in_traffic)

          ;(log/infof "Distance:%s | Duration:%s"distance duration)
          ;(log/infof "Parsed message %s" routes)
          )))
    (catch Exception e
      (log/errorf "!Google location map service:%s"e))))


(defn initialise [text]
  (let [nlu-resp (serv/call-nlu text)
        addr_update (set/rename-keys (into {} nlu-resp) {:1 "start_address", :2 "end_address"})
        watson-connect (do
                         (log/infof "NLU response =>%s|%s"nlu-resp addr_update)
                         (connect text addr_update))]
    (log/infof "Watson Understands => %s"watson-connect)))

(defn -main []
  (initialise "is there any traffic between onajimi street and awoyokun street")

  ;(Thread/sleep 2000)

  ;(connect "Traffic update" {"end_address"   "Victoria Island", "start_address" "Ikeja"})
  ;(call-google "18 onajimi street" "1004 housing estates" "driving" "3rd mainland bridge")
  )
(-main)
;(call-google "1004 housing estate, victoria island" "18 Onajimi Street, Lagos, Nigeria" "driving"  "ikorodu road" )
;(call-google-direction  "1004 housing estate, victoria island" "18 Onajimi Street, Lagos, Nigeria" "driving" "Yaba Bus Stop, Murtala Muhammed Way, Lagos, Nigeria")
;"3rd mainland bridge"