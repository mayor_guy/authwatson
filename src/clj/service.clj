(ns clj.service
  (:require [clojure.walk :as walk]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [org.httpkit.client :as http])
  (:use [clj.rpc]
        [org.httpkit.server]
        [clojure.tools.logging :as log])
  (:import (com.ibm.watson.developer_cloud.natural_language_understanding.v1 NaturalLanguageUnderstanding)
           (com.ibm.watson.developer_cloud.natural_language_understanding.v1.model EntitiesOptions KeywordsOptions
                                                                                   Features AnalysisResults AnalyzeOptions
                                                                                   EntitiesOptions$Builder Features$Builder
                                                                                   KeywordsOptions$Builder AnalyzeOptions$Builder ConceptsOptions$Builder RelationsOptions$Builder)))


;NaturalLanguageUnderstanding service =
;new NaturalLanguageUnderstanding
; (NaturalLanguageUnderstanding.VERSION_DATE_2017_02_27,
;  "{username}",
;  "{password}"
;  )                                                         ;




(defn call-nlu [text]
  (try
    (let [service (NaturalLanguageUnderstanding. "2017-02-27" "5261a01e-9d25-4ebd-b9bb-9f37ce3429c8" "7oKpRynDVCIW")
          entitiesOptions (-> (EntitiesOptions$Builder.) (.emotion (Boolean. true)) (.limit (Integer. 5)) (.build))
          conceptsOptions (-> (ConceptsOptions$Builder.) (.limit (Integer. 5)) (.build))
          relationsOptions (-> (RelationsOptions$Builder.) (.build))
          keywordsOptions (-> (KeywordsOptions$Builder.) (.emotion (Boolean. true)) (.limit (Integer. 5)) (.build))
          features (-> (Features$Builder.) (.entities entitiesOptions) (.keywords keywordsOptions) (.concepts conceptsOptions)
                       (.relations relationsOptions) (.build))

          ;;analyzBuilder (AnalyzeOptions$Builder.)
          ;a_txt (.text analyzBuilder text)
          ;a_features (.features a_txt features)
          ;parameters (.build a_features)
          parameters (-> (AnalyzeOptions$Builder.) (.text text) (.features features)
                         (.build))

          response (.toString  (.execute (.analyze service parameters)))

          resp (json/read-str response :key-fn keyword)
          entities (:entities resp)
          keywords (:keywords resp)
          concepts (:concepts resp)
          relations (:relations resp)
          type (:type (get  entities 0))]
      ;(log/info "Response:"response)
      (log/info "Type:"type)
      (let [params (forloop keywords :text 0)
            entit (forloop entities :text 0)
            concep (forloop concepts :text 0)
            relate (forloop relations :text 0)

            ]
        (log/infof "Entities: %s|%s" (count entities) entit)
        (log/infof "Concepts: %s|%s" (count concepts) concep)
        (log/infof "Relations: %s|%s" (count relations) relate)
        (log/infof "GeographicFeature or location type:%s|%s" (count keywords) params)
        params))

    (catch Exception e
      (log/error "Exception occurs:" e))))

;"is there any traffic between onajimi street and victoria island"
;(call-nlu "what's the traffic situation from Onajimi street to awoyokun")
