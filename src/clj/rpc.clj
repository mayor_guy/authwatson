(ns clj.rpc
  (:require [clojure.tools.logging :as log]
            [clojure.string :as str]))


(defn forloop [data text num]
  (loop [data data
         num num
         result []]
    (let [fdata (first data)
          value (text fdata)]
      (if (empty? data)
        result
        (if (nil? value)
          (recur (rest data) num result)
          (let [num (inc num)]
            (recur (rest data) num (conj result (hash-map (keyword (str num)) value)))))))))

(defn nestedloop [data text subtext num]
  (loop [data data
         num num
         result []
         set_addn [0]]
    (let [fdata (text (first data))
          value (subtext fdata)]
      (if (empty? data)
        set_addn
        (if (nil? value)
          (recur (rest data) num result set_addn)
          (let [num (inc num)
                addn (read-string (get (str/split value #" ") 0))
                new_add (do
                          (log/infof "seen in resp:%s|updated addn:%s"addn (last set_addn))
                          (+ addn (last set_addn)))]
            (log/info "addn2:"new_add)
            (recur (rest data) num
                   (conj result
                         (hash-map (keyword (str num))
                                   value))
                   (conj set_addn
                         new_add))))))))


