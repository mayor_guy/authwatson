(ns clj.autocomplete
  (:require
    [clojure.data.json :as json]
    [org.httpkit.client :as http]
    [clojure.tools.logging :as log])
  (:use [clj.rpc]))


;https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Paris&types=geocode&key=YOUR_API_KEY
(defn show-places [input]
  (try
    (let [url "https://maps.googleapis.com/maps/api/place/autocomplete/json"
          key "AIzaSyAzC0U8qizejJcJAzq4xEmO723ImrO5gXo"
          options  {:query-params {:input input
                                   :types "geocode"
                                   :key key}
                    :headers {"Content-Type" "application/json"}}
          {:keys [status body error] :as trace}  @(http/get url options)]
      (log/infof "Google places request: %s" (apply str url options))
      (if error
        (let []
          (log/infof "Connection exception %s|%s" error trace))

        (let [resp (json/read-str body :key-fn keyword)
              status (:status resp)
              predictions (:predictions resp)
              predictions_row (forloop predictions :description 0)
              end_address (:destination_addresses resp)
              rows (get (:rows resp) 0)
              elements (get (:elements rows) 0)
              distance (:distance elements)
              duration (:duration elements)]
          ;(log/info body)
          (log/infof "%s" status)
          (log/infof "Please confirm the place you were refering to: %s" predictions_row))))
    (catch Exception e
      (log/errorf "!Google location service:%s" e))))

;(show-places "yaba")
;"1004 housing estate, victoria island)